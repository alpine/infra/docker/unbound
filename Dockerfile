FROM alpine

RUN apk add --no-cache unbound openssl && \
	rm -f /etc/unbound/unbound.conf

COPY scripts /usr/local/bin

WORKDIR /etc/unbound

ENTRYPOINT [ "entrypoint.sh" ]
